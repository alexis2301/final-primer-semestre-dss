const { request, response } = require('express')
const pool = require('../database/config')
const utils = require('../resources/utils')
const jwt = require('jsonwebtoken')

const loginForm = (request,response)=>{
    response.render('login')
}

const doLogin = async (request,response) => {
    if(utils.validaInput(request.body.username) && utils.validaInput(request.body.password)){
        const res = await pool.query(
            'select * from users where username = $1 and password = $2',
            [
                request.body.username,
                request.body.password,
            ]
            );
            console.log(res)
            
            if(res.rows[0].id){
                // response.json({message:"Login Exitoso", flag:true})
                user = res.rows[0]
                Object.assign(user, {exp: Math.floor(Date.now() / 1000) + (60*15)})
                const token = jwt.sign(
                    user,
                    process.env.ACCESS_TOKEN_SECRET,
                    )
                    respuesta = {
                        message:"Login Exitoso",
                        userData : JSON.stringify(res.rows[0]),
                        token 
                    }
     
                response.render('recibe_datos',{locals:respuesta})
            }else{
                respuesta = {
                    message:"Login Fallido",
                    userData : '',
                    token : ''
                }
                response.render('error',{locals:respuesta})
            }
    }else{
        respuesta = {
            message:"Login Fallido",
            userData : '',
            token : ''
        }
        response.render('error',{locals:respuesta})
    }
    
}


module.exports ={
    loginForm,
    doLogin,
}