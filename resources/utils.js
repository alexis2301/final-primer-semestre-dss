var sanitizer = require('sanitizer');

const validaInput = (expresion) =>{
    const validadores = ["<script>","#","'--","' --","//","/*","*/",]
    for(var i = 0 ; i< validadores.length;++i){
       if(expresion.includes(validadores[i])){
            return "false"
        }else{
            return "true"
        }
    }
    return expresion
}

module.exports ={
    validaInput,
}