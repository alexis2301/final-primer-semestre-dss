const express = require('express')
const router = express.Router()
const LoginRouter = require('./loginRoutes')


router.use('/login',LoginRouter)

module.exports = router